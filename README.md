# Kellegram's Firefox setup

- [What is this?](#what-is-this)
- [How to setup](#how-to-setup)
- [Extensions](#extensions)
- [Credits](#credits)
- [License](#license)

## What is this?
These are my files and guides related to how I setup my Firefox installation
  
My setup is designed with usability in mind, most ways of breaking sites were removed/modified or will be removed as I figure them out. For an attempt at anonymity use [Tor](https://www.torproject.org/), nothing can replace it in that aspect, you are not anonymous on the internet no matter how much you try with things like this.

---

## How to setup

My user-overrides.js is made using [Arkenfox's user.js](https://github.com/arkenfox/user.js).

Please read their [wiki](https://github.com/arkenfox/user.js/wiki) for a lot of useful information.

This is not a comprehensive guide, I assume you know at least some basic usage of the system you are using and/or some basic google skills.

1. Set up the `policies.json` file
   - Create a folder called `distribution` in the same folder as `Firefox` binary
   - Move the `policies.json` file to that folder
2. In Firefox, ideally create a new, **clean** profile*
   - Go to `about:profiles`
   - `Create a new profile`
   - In "Root directory" line select `Open folder`, this is where you put `user-overrides.js` in 
3. Clone [Arkenfox's repo](https://github.com/arkenfox/user.js)
4. Copy `user.js`, `updater`(.sh for linux, .bat for Windows) and `prefsCleaner` (.sh or .bat) to the profile root folder
5. Run `updater` (On linux you might have to `chmod +x *.sh` first) 
6. You can now launch Firefox and setup as you like there

\* If you want to keep your existing profile, the Arkenfox wiki I linked explains how to do some cleanup to avoid issues, go and read it
*I always delete the pre-installed profiles (default and default-release) and create a new clean profile for if I need to verify that some issue is caused by my hardened one*

## Extensions
- [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
- [Bitwarden](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/)
- [SponsorBlock](https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/)
- [Improve YouTube!](https://addons.mozilla.org/en-US/firefox/addon/youtube-addon/)
- [Skip Redirect](https://addons.mozilla.org/en-US/firefox/addon/skip-redirect/)
- [Search by Image](https://addons.mozilla.org/en-US/firefox/addon/search_by_image/)
- [Stylus](https://addons.mozilla.org/en-US/firefox/addon/styl-us/)




## Credits
- My user-overrides.js is made using [Arkenfox's user.js](https://github.com/Arkenfox/user.js).
    - Please read their [wiki](https://github.com/Arkenfox/user.js/wiki) for a lot of useful information.


## License
![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)
To the extent possible under law, [Kellegram](https://www.kellegram.xyz) has waived all copyright and related or neighboring rights to Kellegram's Firefox setup.
